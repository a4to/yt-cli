
PKG = yt-cli
INFO = $(shell uname -a)

all: info

yt-cli:
	@cd usr/lib/node_modules/"${PKG}" && \
		npm install -g --unsafe-perm && \
		npm link

install: yt-cli

clean:
	@rm -rf ${PKGDIR}/usr/lib/node_modules/"${PKG}"

info:
	@echo -e "\n\e[1;33m[*]\e[0m ${INFO}"

uninstall: clean
	@echo -e "\n\e[1;32m[+]\e[0m ${PKG} uninstalled"

remove: uninstall

.PHONY: all install clean info uninstall remove

