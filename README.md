# YT-CLI - YouTube Command Line Interface

### Welcome to YT-CLI, a straightforward CLI tool for downloading YouTube videos. <br>
Designed to be fast, efficient, and user-friendly, YT-CLI operates with precision and speed.

*Version: 1.0.0*

***

## Key Features

+ **Interactive Selection**: No flags needed, provides a list for selection of the correct video.
+ **Auto-Select**: Automatically selects the first result with `-a` flag.
+ **Download Video**: Download YouTube video with `-d` flag.
+ **Custom Directory**: Specify a destination directory with `-t` when used with `-d`.
+ **Direct Link**: Download from a direct link or URL with `-l` flag.
+ **Help**: Use `-h` for help information.

## Requirements

+ Node.js
+ npm

## Installation

### **P-Track** can be acquired from various platforms:

### **NPM**:
    npm install -g yt-cli

### **Arch User Repository (AUR)**:
#### For Arch Linux users, yt-cli is available from on the AUR for all Arch or Arch based distro with the help of your favorite AUR helper, eg:
    yay -Sy yt-cli



## Usage
    yt-cli [options]

## Options

+ `-a`: Auto select first result.
+ `-d`: Download video.
+ `-t`: Used with `-d`, allows user to provide destination directory.
+ `-l`: Provide direct link or URL to video.
+ `-h`: Display help information.

## Examples

    # Auto-select and download first result
    yt-cli -a -d

    # Download video to a custom directory
    yt-cli -d -t /path/to/directory

    # Download from a direct link or URL
    yt-cli -l https://www.youtube.com/watch?v=example


***

## License

yt-cli is open source and licenced under the MIT/X Consortium license

## Support
If you encounter any problems or have suggestions for yt-cli, please open an issue on GitLab. We value your feedback and will respond as quickly as possible.

